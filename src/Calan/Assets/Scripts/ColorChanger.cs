using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{
    public float changeRate;
    public enum ColorSelector
    {
        Green,//0
        Red,//1
        Blue,//2
    }

    [SerializeField] private ColorSelector currentColor;
    [SerializeField] private int direction;
    public ColorSelector startColor;

    public GameObject particleColor;
    private ParticleSystem.MainModule main;
    
    // Start is called before the first frame update
    void Start()
    {
        main = particleColor.GetComponent<ParticleSystem>().main;
        currentColor = startColor;
        direction = 1;
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentColor)
        {
            case ColorSelector.Green:
                float tmp = main.startColor.color.g;
                tmp = tmp + (changeRate * direction * Time.deltaTime);
                if(tmp >= 1.0f)
                {
                    tmp = 1.0f;
                    nextColor();
                }
                else if (tmp <= 0.0f)
                {
                    tmp = 0.0f;
                    nextColor();
                }
                main.startColor = new Color(main.startColor.color.r, tmp, main.startColor.color.b, main.startColor.color.a);
               
                break;
            case ColorSelector.Red:
                float tmp1 = main.startColor.color.g;
                tmp1 = tmp1 + (changeRate * direction * Time.deltaTime);
                if (tmp1 >= 1.0f)
                {
                    tmp1 = 1.0f;
                    nextColor();
                }
                else if (tmp1 <= 0.0f)
                {
                    tmp1 = 0.0f;
                    nextColor();
                }
                main.startColor = new Color(tmp1, main.startColor.color.g, main.startColor.color.b, main.startColor.color.a);                
                break;
            case ColorSelector.Blue:
                float tmp2 = main.startColor.color.g;
                tmp2 = tmp2 + (changeRate * direction * Time.deltaTime);
                if (tmp2 >= 1.0f)
                {
                    tmp2 = 1.0f;
                    nextColor();
                }
                else if (tmp2 <= 0.0f)
                {
                    tmp2 = 0.0f;
                    nextColor();
                }
                main.startColor = new Color(main.startColor.color.r, main.startColor.color.g, tmp2, main.startColor.color.a);                
                break;
        }
    }

    private void nextColor()
    {
        switch (currentColor)
        {
            case ColorSelector.Green:
                currentColor = ColorSelector.Red;
                direction = direction * -1;
                break;
            case ColorSelector.Red:
                currentColor = ColorSelector.Blue;
                direction = direction * -1;
                break;
            case ColorSelector.Blue:
                currentColor = ColorSelector.Green;
                direction = direction * -1;
                break;

        }
    }
}
