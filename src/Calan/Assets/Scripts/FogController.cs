using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogController : MonoBehaviour
{
    public int fogCounter = 0;
    public static FogController Instance;
    public GameObject fogExtender;

    private void Awake()
    {
        Instance = this;
    }

    public void addFog()
    {
        fogCounter = fogCounter + 1;
    }
    public void subtractFog()
    {
        fogCounter = fogCounter - 1;
    }

    public void spawnFogExtender(Vector3 _pos)
    {
        Instantiate(fogExtender, _pos, Quaternion.identity);
    }

    public bool isVisible()
    {
        return fogCounter > 0;
    }
}
