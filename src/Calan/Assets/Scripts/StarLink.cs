using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarLink : MonoBehaviour
{
    public Galaxy StartPoint;
    public Galaxy EndPoint;
    public SpaceShipSpawner Spawner;

    public float CreationTime;
    private bool isRemoved = false;



    // Start is called before the first frame update
    void Awake()
    {
        CreationTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
    }

    void FixedUpdate() {
    }

    public bool HasSameOriginPlanet(StarLink other) {
        return StartPoint == other.StartPoint || StartPoint == other.EndPoint || EndPoint == other.StartPoint || EndPoint == other.EndPoint;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        // Collisions allowed, if just colliding because of origin from same planet.
        // Remove Link if not allowed to place
        var possibleLink = other.gameObject.GetComponent<StarLink>();
        if (possibleLink != null) {
            // Remove only newer starlink, else existing get deleted too.
            if (!HasSameOriginPlanet(possibleLink) && CreationTime > possibleLink.CreationTime) {
                Remove();
            }
        }
        // No Collision allowed if planet in the way or crossing other starlink.
        else if (other.gameObject.GetComponent<Galaxy>()) {
            Remove();
        }
        else if (other.gameObject.GetComponent<StarLinkIndicator>()) {
            //pass
        }
        else if (other.gameObject.tag == "NoSpawnRadius") {
            //pass
        }
        else {
            Debug.LogError("Unknown collision with " + other.gameObject.name);
        }
    }

    public void CreateBetween(Galaxy galaxy1, Galaxy galaxy2) {
        StartPoint = galaxy1;
        EndPoint = galaxy2;

        Create();           
    }

    public void Remove() {
        if (isRemoved) {
            return;
        }
        CancelInvoke("HandleSuccessfullLink");
        isRemoved = true;
        // not needed anymore because planets cant be disconnected
        // StartPoint.DisconnectPlanet(EndPoint);
        // EndPoint.DisconnectPlanet(StartPoint);
        // Destroy(Spawner.gameObject);
        
        Destroy(gameObject);
    }

    private void HandleSuccessfullLink() {
        if (!EndPoint.IsConnectedToOtherPlanet() && GameController.Instance.FogEnabled)
        {
            FogController.Instance.spawnFogExtender(EndPoint.transform.position);
        }
        StartPoint.ConnectPlanet(EndPoint);
        EndPoint.ConnectPlanet(StartPoint);
        Spawner = Instantiate<SpaceShipSpawner>(GameController.Instance.SpawnerPrefab);
        Spawner.Galaxy1 = StartPoint;
        Spawner.Galaxy2 = EndPoint;
        GameController.Instance.RegisterSuccessfullLink();

    }

    private void Create() {
        if (StartPoint == EndPoint) {
            Debug.LogError("StarLink Start and Endpoint are the same (Disallowed). Link gets destroyed");
            Remove();
        }
        //For creating line renderer object
        var lineRenderer = gameObject.AddComponent<LineRenderer>();
        lineRenderer.material = GameController.Instance.StarLinkMaterial;
        var color = Color.white;
        color.a = 0.1f;
        lineRenderer.startColor = color;
        lineRenderer.endColor = color;
        lineRenderer.colorGradient.mode = GradientMode.Fixed;
        lineRenderer.startWidth = 0.1f;
        lineRenderer.endWidth = 0.1f;
        lineRenderer.positionCount = 2;
        lineRenderer.useWorldSpace = false;

                        
        //For drawing line in the world space, provide the x,y,z values
        var startPosition = StartPoint.transform.position;
        var endPosition = EndPoint.transform.position;
        var offset = 3f;
        var lineStart = startPosition + (endPosition - startPosition).normalized * offset * StartPoint.Scale;
        var lineEnd = endPosition - (endPosition - startPosition).normalized * offset * EndPoint.Scale;
        lineRenderer.SetPosition(0, lineStart); //x,y and z position of the starting point of the line
        lineRenderer.SetPosition(1, lineEnd); //x,y and z position of the end point of the line
    
        // Create Polygon collider;
        var collider = gameObject.AddComponent<PolygonCollider2D>();
        collider.isTrigger = true;
        var collideroffset = Vector2.Perpendicular(endPosition - startPosition).normalized * 0.005f;
        collider.points = new Vector2[] {
            new Vector2(lineStart.x, lineStart.y) - collideroffset ,
            new Vector2(lineStart.x, lineStart.y) + collideroffset ,
            new Vector2(lineEnd.x, lineEnd.y) + collideroffset ,
            new Vector2(lineEnd.x, lineEnd.y) - collideroffset 
        };


        // Create rigid body for collision detection.
        var rb = gameObject.AddComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        Invoke("HandleSuccessfullLink", 0.15f);
    }
}
