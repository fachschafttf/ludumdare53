using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChangeVolume : MonoBehaviour
{
    public Scrollbar Sounds;
    public Scrollbar Music;
    public TextMeshProUGUI MusicVolumeDisplay;
    public TextMeshProUGUI SoundVolumeDisplay;

    void Start()
    {
        Music.onValueChanged.AddListener(delegate { ValueChangeCheckMusic(); });
        Sounds.onValueChanged.AddListener(delegate { ValueChangeCheckSounds(); });
    }
    // Update is called once per frame
    void ValueChangeCheckMusic()
    {
        MusicVolumeDisplay.text = Math.Round(Music.value * 100).ToString();
    }

        void ValueChangeCheckSounds()
    {
        SoundVolumeDisplay.text = Math.Round(Sounds.value * 100).ToString();
    }
}
