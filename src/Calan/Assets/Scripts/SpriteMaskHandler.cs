using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMaskHandler : MonoBehaviour
{
    private bool inside;
    private bool lastFrame;

    private void Start()
    {
        inside = false;
        lastFrame = false;
    }
    private void Update()
    {
        if (Vector3.Distance(this.transform.position, Camera.main.ScreenToWorldPoint(Input.mousePosition)) < this.transform.localScale.x * 0.55f)
        {
            inside = true;
        }
        else
        {
            inside = false;
        }

        if(inside != lastFrame)
        {
            if (GameController.Instance.FogEnabled) {
                if (inside)
                {
                    FogController.Instance.addFog();
                }
                else
                {
                    FogController.Instance.subtractFog();
                }
            }
                
        }
        lastFrame = inside;
    }
}
