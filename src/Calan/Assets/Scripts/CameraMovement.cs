using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private Vector3 mousePosition;
    private Vector2 screenSize;
    private Vector3 newPosition;
    private float fieldOfView;

    public float PanningSpeed = 0.01f;
    
    public float BackgroundScrollingSpeed = 1f;
    public SpriteRenderer Background;

    void Awake()
    {

    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        fieldOfView = Camera.main.orthographicSize;
        if (Input.mouseScrollDelta.y < 0)
        {
            fieldOfView = math.min(fieldOfView + 2, 40);
        } else if (Input.mouseScrollDelta.y > 0)
        {
            fieldOfView = math.max(fieldOfView - 2, 1);
        }
        mousePosition = Input.mousePosition;
        screenSize = new Vector2(Screen.width, Screen.height);
        newPosition = new Vector3(0,0,0);
        if (!GameController.Instance.IsPaused) {
            if (mousePosition.x > screenSize.x * 0.9 || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                newPosition.x += PanningSpeed * fieldOfView;
            } 
            if (mousePosition.x < screenSize.x * 0.1 || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                newPosition.x += -PanningSpeed * fieldOfView;
            }
            if (mousePosition.y > screenSize.y * 0.9 || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                newPosition.y += PanningSpeed * fieldOfView;
            } 
            if (mousePosition.y < screenSize.y * 0.1 || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                newPosition.y += -PanningSpeed * fieldOfView;
            }
        }
        gameObject.transform.position += newPosition;
        Camera.main.orthographicSize = fieldOfView;
        // Parallax background
        ScrollBackground();
    }

    private void ScrollBackground() {
        Vector2 offset = new Vector2(transform.position.x, transform.position.y) * BackgroundScrollingSpeed;
        if (Background != null) {
            Background.material.mainTextureOffset = offset;
        }
    }
}
