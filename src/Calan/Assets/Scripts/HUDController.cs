using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;

public class HUDController : MonoBehaviour
{

    public GameObject PrefabResourceDisplay;
    public List<GameObject>LinkDisplays = new List<GameObject>();
    public GameObject LinkDisplayPrefab;
    // Start is called before the first frame update
    void Start()
    {
        CreateResourceDisplays();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateLinksAvailable(int links_left, int overall) {
        var current = LinkDisplays.Count;
        var difference = links_left - current;

        if (difference > 0) {
            for (var i = 0; i < difference; i++) {
                var display = Instantiate(LinkDisplayPrefab, transform.position, Quaternion.identity);
                display.transform.SetParent(GameObject.Find("LinksAvailable").transform);
                display.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(50, -100 - (i + current) * 75, 0);
                LinkDisplays.Add(display);
            }
        }
        else if (difference < 0) {
            for (var i = 0; i > difference; i--) {
                if (LinkDisplays.Count == 0) {
                    continue;
                }
                var display = LinkDisplays[LinkDisplays.Count - 1];
                LinkDisplays.Remove(display);
                Destroy(display);
            }
        }

        transform.Find("LinksAvailable").GetComponent<TextMeshProUGUI>().text = links_left.ToString() + " links left";
        transform.Find("LinksEstablished").GetComponent<TextMeshProUGUI>().text = "Links: " + overall.ToString();       
    }

    public void IncreaseResourceDisplay(string resourceName, int value = 1) {
        var textfield = transform.Find("Display_" + resourceName).Find("Value").GetComponent<TextMeshProUGUI>();
        var currentValue = int.Parse(textfield.text);
        textfield.text = (currentValue + value).ToString();
    }

    public void UpdateScore(int value) {
        transform.Find("Score").GetComponent<TextMeshProUGUI>().text = "Score: " + value.ToString();
    }

    void CreateResourceDisplays() {
        
        var resources = Resources.LoadAll<ResourceObject>("IngameResources");
        var index = 0;
        foreach (var resource in resources) {
            var display = Instantiate(PrefabResourceDisplay, Vector3.zero, Quaternion.identity);
            display.name = "Display_" + resource.resourceName;
            display.transform.SetParent(transform);
            display.transform.GetComponent<RectTransform>().anchoredPosition = new Vector3(-300 + index * 100, 495, 0);
            display.transform.Find("Icon").GetComponent<Image>().sprite = resource.icon;
            display.transform.Find("Value").GetComponent<TextMeshProUGUI>().text = "0";
            index++;
        }

    }
}
