using UnityEngine;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine.Rendering;


public class Galaxy : MonoBehaviour
{

    private List<Galaxy> ConnectedPlanets = new List<Galaxy>();
    public ToolTip toolTip;

    public SO_Planet PlanetData;

    public float Scale;
    

    // Start is called before the first frame update
    void Start()
    {
        Scale = Random.Range(0.5f, 1.5f);
        SetPlanetData();
        AddCollider();        
        AddResources();
        transform.localScale = Vector3.one * Scale;

    }

    private void AddResources() {
        // Generate 3 different Input Resources and 1 Output Resource.
        var availableResources = new List<ResourceObject>( GameController.Instance.PlanetResources);
        var outputResource = PlanetData.OutputResource;
        // ourput Resource should not be requested of planet.
        availableResources.Remove(outputResource);
        var planetResources = new List<ResourceObject>();
        for (var i = 0; i < 3; i++) {
            var selectedResource = availableResources[Random.Range(0, availableResources.Count)];
            planetResources.Add(selectedResource);
            availableResources.Remove(selectedResource);
        }
        GetComponent<IngameResources>()?.InitializeResources(planetResources, outputResource, Scale);
    }

    public bool IsConnectedToOtherPlanet() {
        return ConnectedPlanets.Count > 0;
    }

    private void SetPlanetData() {
        // Choose Random if not set found.
        if (PlanetData == null) {
            var planetDatas = Resources.LoadAll<SO_Planet>("Planets/"); 
            PlanetData = planetDatas[Random.Range(0, planetDatas.Length)];
        }
        var spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = PlanetData.Texture;
        if (GameController.Instance.FogEnabled) {
            spriteRenderer.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        }
    }

    private void AddCollider() {
        var collider = gameObject.AddComponent<CircleCollider2D>();
        collider.radius = 2;
    }

    public void ConnectPlanet(Galaxy galaxy) {
        if (ConnectedPlanets.Contains(galaxy)) {
            Debug.LogError("Planet already registered as connected while attempting to connect. This should not happen");
        }
        else {
            ConnectedPlanets.Add(galaxy);
            this.gameObject.GetComponent<IngameResources>().UpdateConnection(galaxy.gameObject.GetComponent<IngameResources>());
        }
    }

    public void DisconnectPlanet(Galaxy galaxy) {
        if (!ConnectedPlanets.Contains(galaxy)) {
            Debug.LogError("Planet not registered as connected while attempting to disconnect. This should not happen");
        }
        else {
            ConnectedPlanets.Remove(galaxy);
        }
    }


    // Update is called once per frame
    void Update()
    {
    }

    void OnMouseDown() {
        if (GameController.Instance.FogEnabled && !FogController.Instance.isVisible())
        {
            return;
        }
        GameController.Instance.RegisterGalaxyClick(this);
    }

    void OnMouseEnter() {
        if (GameController.Instance.FogEnabled && !FogController.Instance.isVisible())
        {
            return;
        }
        //Debug.Log(FogController.Instance.fogCounter);
        GameController.Instance.HoveredPlanet = this;
        if (!GameController.Instance.IsPaused) {
            toolTip.Show();
        }
    }

    void OnMouseExit() {
        GameController.Instance.HoveredPlanet = null;
        toolTip.Hide();
    }

    public void ChangeAlpha() {
        SetAlpha(0.5f);
    }

    public void ResetAlpha() {
        SetAlpha(1f);
    }

    private void SetAlpha(float alpha) {
        var renderer = gameObject.GetComponent<SpriteRenderer>();
        var color = renderer.color;
        color.a = alpha;
        renderer.color = color;
    }



    public List<Galaxy> getConnectedPlanets()
    {
        return ConnectedPlanets;
    }
}
