using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IngameResources : MonoBehaviour
{
    //fields
    [SerializeField] private int resourceLevel;
    [SerializeField] private int planetWorth;
    private int maxResourceLevel = 3;
    //public Vector3[] positionsL;
    //public Vector3[] positionsR;

    //public GameObject placeHolderSprite;
    //public float offset = 0.05f;

    public List<ResourceObject> inputResources;
    public ResourceObject outputResource;
    public List<ResourceObject> availableIn;
    public int availableOut = 1;

    private float _planetScale = 1;

    private List<GameObject> outputIndicators = new List<GameObject>();


    public bool fillByHand;
    public List<ResourceObject> _in;
    public ResourceObject _out;

    //public List<GameObject> resourceSprites;

    // Start is called before the first frame update

    void Awake() {
        //resourceSprites = new List<GameObject>();
        resourceLevel = 1;
        availableOut = 1;
        planetWorth = resourceLevel * resourceLevel;
        inputResources =  new List<ResourceObject>();

    }
    void Start()
    {
        if (fillByHand)
        {
            InitializeResourcesByHand();
            //Draw();
        }
    }

    public void InitializeResources(List<ResourceObject> _input, ResourceObject _output, float planetScale)
    {
        inputResources = _input;
        outputResource = _output;
        _planetScale = planetScale;
        availableIn = new List<ResourceObject>(inputResources);

        InitializeInputIndicators();
        InitializeOutputIndicators();
        
        
    }

    private void InitializeInputIndicators() {// Add Indicators for input and output
        var distance = 4f;
        var degreeInterval = 30f*Mathf.Deg2Rad;
        var startDegree = 180*Mathf.Deg2Rad - degreeInterval;
        foreach (var resource in availableIn) {
            var offset = new Vector3(Mathf.Cos(startDegree) * distance, Mathf.Sin(startDegree)* distance);

            AddIndicator(resource, "InputIndicator_", offset);
            startDegree +=degreeInterval;
        }

    }

    private GameObject AddIndicator(ResourceObject resource, string prefix, Vector3 offset) {
            var indicator = new GameObject();
            var spriteRenderer = indicator.AddComponent<SpriteRenderer>();
            spriteRenderer.sprite = resource.circleIcon;
            if (GameController.Instance.FogEnabled) {
                spriteRenderer.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
            }
            
            indicator.transform.position += transform.position + offset;
            indicator.transform.SetParent(transform);
            indicator.transform.localScale = Vector3.one / _planetScale;
            indicator.name = prefix + resource.resourceName;
            return indicator;
    }

    public void InitializeOutputIndicators() {
        RemoveOutputIndicators(outputResource.resourceName);
        var distance = 4f;
        var degreeInterval = 30f*Mathf.Deg2Rad;
        var startDegree = 0*Mathf.Deg2Rad + (availableOut - 1) * 0.5f * degreeInterval;

        for (var i = 0; i < availableOut; i++) 
        {
            var offset = new Vector3(Mathf.Cos(startDegree) * distance, Mathf.Sin(startDegree)* distance);

            outputIndicators.Add(AddIndicator(outputResource, "OutputIndicator_", offset));
            startDegree -=degreeInterval;
        }
    }

    private void RemoveOutputIndicators(string resourceName) {
        foreach(var indicator in outputIndicators) {
            Destroy(indicator);
        }
        outputIndicators.Clear();
    }
    

    void InitializeResourcesByHand()
    {
        inputResources = _in;
        outputResource = _out;
        availableIn = new List<ResourceObject>(inputResources);
    }

    //Call when a new Connection
    public void UpdateConnection(IngameResources other)
    {
        if (availableIn.Contains(other.outputResource) && other.availableOut > 0)
        {
            availableIn.Remove(other.outputResource);
            transform.Find("InputIndicator_"+ other.outputResource.resourceName).gameObject.SetActive(false);
            other.availableOut = other.availableOut - 1;
            
            other.IconCanvasAnimation();
            other.InitializeOutputIndicators();
            GameController.Instance.ResourceFullfilled(other.outputResource.resourceName);
            DoLevelUp();
        }      
    }
    public void UpdateAllConnection()
    {
        List<Galaxy> otherGalxys = this.gameObject.GetComponent<Galaxy>().getConnectedPlanets();
        foreach (Galaxy gal in otherGalxys)
        {
            UpdateConnection(gal.gameObject.GetComponent<IngameResources>());
            gal.gameObject.GetComponent<IngameResources>().UpdateConnection(this);
        }
    }

    private void DoLevelUp()
    {
        resourceLevel = resourceLevel + 1;
        availableOut = availableOut + 1;
        planetWorth = resourceLevel * resourceLevel;
        ScoreUpdate(planetWorth);
        InitializeOutputIndicators();
        UpdateAllConnection();

        //Draw();
    }

    private void ScoreUpdate(int points) {
        for (var i = 0; i < points; i++) {
            Invoke("SingleScoreUpdate", 0.1f * i);
        }
    }

    private void SingleScoreUpdate() {        
        GameController.Instance.AddToScore(1);
        var icon = Instantiate(GameController.Instance.UiMovingIconFrefab,transform.position, Quaternion.identity);
        icon.transform.SetParent(GameObject.Find("Canvas").transform);
        icon.StartLocation = Camera.main.WorldToScreenPoint(transform.position);
        icon.EndLocation = GameObject.Find("Score").transform.position;

        icon.GetComponent<Image>().sprite = GetComponent<Galaxy>().PlanetData.Texture;
    }



    private void IconCanvasAnimation() {        
        var icon = Instantiate(GameController.Instance.UiMovingIconFrefab,transform.position, Quaternion.identity);
        icon.transform.SetParent(GameObject.Find("Canvas").transform);
        icon.StartLocation = Camera.main.WorldToScreenPoint(transform.position);
        icon.EndLocation = GameObject.Find("Display_" + outputResource.resourceName).transform.Find("Icon").transform.position;

        icon.GetComponent<Image>().sprite = outputResource.circleIcon;
    }

   
    //public void Draw()
    //{
    //    DespawnResourceIcons();
    //    for (int i = 0; i < resourceLevel; i++)
    //    {
    //        for (int j = 0; j < inputResources[i].Count; j++)
    //        {
    //            GameObject temp = Instantiate(placeHolderSprite, transform.position + positionsL[i] + j * offset * new Vector3(1,0,0) , Quaternion.identity);
    //            temp.GetComponent<SpriteRenderer>().sprite = inputResources[i][j].artwork;
    //            resourceSprites.Add(temp);
    //        }
    //        for (int j = 0; j < outputResources[i].Count; j++)
    //        {
    //            GameObject temp = Instantiate(placeHolderSprite, transform.position + positionsR[i] + j * offset * new Vector3(-1, 0, 0), Quaternion.identity);
    //            temp.GetComponent<SpriteRenderer>().sprite = outputResources[i][j].artwork;
    //            resourceSprites.Add(temp);
    //        }
    //    }
    //}
    
    //void DespawnResourceIcons()
    //{
    //    for (int i = 0; i < resourceSprites.Count; i++)
    //    {
    //        Destroy(resourceSprites[i]);
    //    }
    //    resourceSprites = new List<GameObject>();
    //}
    //Resources visible
    //Update Connection
}
