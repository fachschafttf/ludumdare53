using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetSpawner : MonoBehaviour
{

    public float MinDistance = 20f;

    public float SpawnRadius = 300f;

    public GameObject PlanetPrefab;
    private GameObject _planetContainer;

    private bool _initSpawnDone;
    public bool SpawnGuardActive;
    public Vector3 GuardMinimumPosition;
    public Vector3 GuardMaximumPosition;
    
    // Start is called before the first frame update
    void Start()
    {
        _planetContainer = new GameObject();
        _planetContainer.name = "SpawnedPlanetContainer";
        InitialSpawn();

    }
        // Update is called once per frame
    void Update()
    {
        //Follow Camera
        var position = Camera.main.transform.position;
        position.z = 0;
        transform.position = position;
        UpdateSpawnGuard();
    }

    private void InitSpawnGuard() {
        RemoveCollidingPlanets();
        _initSpawnDone = true;
        GuardMinimumPosition = new Vector3(-SpawnRadius / 2, -SpawnRadius / 2, 0);
        GuardMaximumPosition = new Vector3(SpawnRadius / 2, SpawnRadius / 2, 0);
        InvokeRepeating("TrySpawn", 0f, 0.01f);
    }

    private void RemoveCollidingPlanets() {
        foreach (var planet in FindObjectsOfType<Galaxy>()) {
            var colliders = Physics2D.OverlapCircleAll(planet.transform.position, MinDistance/2);

            if (colliders.Length > 1) {
                Destroy(planet.gameObject);
            }
        }
    }

    private void UpdateSpawnGuard() {
        if (!_initSpawnDone) {
            return;
        }
        GuardMinimumPosition = new Vector3(Mathf.Min(transform.position.x-SpawnRadius / 2, GuardMinimumPosition.x), Mathf.Min(transform.position.y-SpawnRadius / 2, GuardMinimumPosition.y), 0);
        GuardMaximumPosition = new Vector3(Mathf.Max(transform.position.x+SpawnRadius / 2, GuardMaximumPosition.x), Mathf.Max(transform.position.y+SpawnRadius / 2, GuardMaximumPosition.y), 0);

    }

    void InitialSpawn() {
        var tries = 500;
        for (var i = 0; i < tries; i++) {
            Invoke("TrySpawn", 0.001f*i);
        }
        Invoke("InitSpawnGuard", 0.002f*tries);
    }

    private bool PositionInSpawnGuard(Vector3 position) {
        return SpawnGuardActive && position.x > GuardMinimumPosition.x && position.x < GuardMaximumPosition.x && position.y > GuardMinimumPosition.y && position.y < GuardMaximumPosition.y;
    }

    private void TrySpawn() {
        var positions = CreateSpawnPositions();
        foreach(var position in positions) {
            var pos = position;
            pos.z = 0;
            if (PositionInSpawnGuard(pos)){
                continue;
            }
            var collider = Physics2D.OverlapCircle(pos, MinDistance);

            if (collider == null) {
                var planet = Instantiate(PlanetPrefab, pos, Quaternion.identity);
                planet.transform.SetParent(_planetContainer.transform);
            }
        }
    }

    private List<Vector3> CreateSpawnPositions() {
        var position = new List<Vector3>();
        if (!_initSpawnDone) {
            position.Add(transform.position + new Vector3(Random.Range(-SpawnRadius, SpawnRadius),Random.Range(-SpawnRadius, SpawnRadius), 0));
        }
        else {
            // Top
            position.Add(transform.position + new Vector3(Random.Range(-SpawnRadius, SpawnRadius), Random.Range(-SpawnRadius, -SpawnRadius / 2)));
            // Bottom
            position.Add(transform.position + new Vector3(Random.Range(-SpawnRadius, SpawnRadius), Random.Range(SpawnRadius / 2, SpawnRadius)));
            // Left
            position.Add(transform.position + new Vector3(Random.Range(-SpawnRadius, -SpawnRadius / 2), Random.Range(-SpawnRadius, SpawnRadius)));
            // Right
            position.Add(transform.position + new Vector3(Random.Range(SpawnRadius / 2, SpawnRadius), Random.Range(-SpawnRadius, SpawnRadius)));

        }
        
        

        return position;

    }


}
