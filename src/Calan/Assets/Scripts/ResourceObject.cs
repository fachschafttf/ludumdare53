using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new resources", menuName = "resource")]
public class ResourceObject : ScriptableObject
{
    public string resourceName;
    public TextAsset description;
    public Sprite artwork;
    public Sprite icon;
    public Sprite circleIcon;
}
