using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarLinkIndicator : MonoBehaviour
{
    public Material LineMaterial;
    private LineRenderer _lineRenderer;
    private PolygonCollider2D _collider;
    public List<GameObject> _collidingObjects = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.material = GameController.Instance.StarLinkMaterial;
        _lineRenderer.startWidth = 0.1f;
        _lineRenderer.endWidth = 0.1f;
        _lineRenderer.positionCount = 2;
        _lineRenderer.useWorldSpace = false;
        // Create rigid body for collision detection.
        var rb = gameObject.AddComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        rb.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        // Create Polygon collider;
        _collider = gameObject.AddComponent<PolygonCollider2D>();
        _collider.isTrigger = true;
    
    }

    // Update is called once per frame
    void Update()
    {       
        Draw();
    }

    
    private void OnTriggerEnter2D(Collider2D other) {
        _collidingObjects.Add(other.gameObject);
    }

    private void OnTriggerExit2D(Collider2D other) {
        _collidingObjects.Remove(other.gameObject);
    }

    private bool IsColliding() {
        foreach(var other in _collidingObjects) {
            if (other.tag != "NoSpawnRadius" && other != GameController.Instance.HoveredPlanet?.gameObject) {
                return true;
            }
        }
        return false;
    }

    private void Draw() {

        // TODO: This is very hacky and ungeneric
        var lineStart = new Vector3(0, 0, 0);
        var lineEnd = new Vector3(0, 0, 0);
        _lineRenderer.startColor = _lineRenderer.endColor = IsColliding() ?  Color.red : Color.blue;
        if (GameController.Instance.CurrentyClickedGalaxy != null) {
        //For drawing line in the world space, provide the x,y,z values
            var startPosition = GameController.Instance.CurrentyClickedGalaxy.transform.position;
            var endPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            endPosition.z = 0;
            var offset = 3f;
            var direction = endPosition - startPosition;
            lineStart = startPosition + (endPosition - startPosition).normalized * offset * GameController.Instance.CurrentyClickedGalaxy.Scale;
            lineEnd = direction.magnitude < offset ? lineStart : endPosition - (endPosition - startPosition).normalized * 0.1f * offset;
        }
        else {
            _collidingObjects.Clear();
        }
        _lineRenderer.SetPosition(0, lineStart); //x,y and z position of the starting point of the line
        _lineRenderer.SetPosition(1, lineEnd); //x,y and z position of the end point of the line    

        var collideroffset = Vector2.Perpendicular(lineStart - lineEnd).normalized * 0.005f;
        _collider.points = new Vector2[] {
            new Vector2(lineStart.x, lineStart.y) - collideroffset ,
            new Vector2(lineStart.x, lineStart.y) + collideroffset ,
            new Vector2(lineEnd.x, lineEnd.y) + collideroffset ,
            new Vector2(lineEnd.x, lineEnd.y) - collideroffset 
        };


    }
}
