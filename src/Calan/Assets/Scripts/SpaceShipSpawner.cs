using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShipSpawner : MonoBehaviour
{
    public SpaceShip PrefabSpaceShip;
    public Galaxy Galaxy1;
    public Galaxy Galaxy2;
    public float SpawnFrequency = 0.2f;
    private Sprite[] sprites;
    // Start is called before the first frame update
    void Start()
    {
        sprites = Resources.LoadAll<Sprite>("Ships/");
        Invoke("SpawnShip", SpawnFrequency);
    }

    private void SpawnShip()
    {
        if (GameController.Instance.IsPaused) { return;}
        var ship1 = Instantiate(PrefabSpaceShip, Galaxy1.transform.position, Quaternion.identity);
        var ship2 = Instantiate(PrefabSpaceShip, Galaxy2.transform.position, Quaternion.identity);
        
        ship1.StartLocation = ship2.EndLocation = Galaxy1.transform.position;
        ship2.StartLocation = ship1.EndLocation = Galaxy2.transform.position;

        ship1.GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length)];
        ship2.GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length)];
        
        Invoke("SpawnShip", SpawnFrequency);
    }
}
