using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using TMPro;
using UnityEditor.Rendering.Universal;
using UnityEngine;
using UnityEngine.UI;


public class ToolTip : MonoBehaviour
{
    public Image texture;
    public TextMeshProUGUI text;
    private Galaxy parent;
    private IngameResources parentRessouces;
    private List<GameObject> requiredRessources = new();
    private ResourceObject outPutResource;
    private GameObject outPutGO;
    void Start()
    {
        parent = transform.parent.GetComponent<Galaxy>();
        parentRessouces = parent.GetComponent<IngameResources>();
        outPutResource = parentRessouces.outputResource;
        texture.rectTransform.sizeDelta = new Vector2(130, 160);
        ResetIcons();
    }
    void Update()
    {
        if (!gameObject.activeSelf) { return;}
        var basePosition = Input.mousePosition;
        texture.transform.position = basePosition + new Vector3(85, -30, 0);
        text.transform.position = basePosition + new Vector3(130, 20, 0);
        outPutGO.transform.position = basePosition + new Vector3(75, -65, 0);
        var Offset = new Vector3(40, 0, 0);
        int index = 1;
        foreach (var image in requiredRessources)
        {
            image.transform.position = basePosition + new Vector3(5, 0, 0) + index * Offset;
            index++;
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
        ResetIcons();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    private void AppendIcons(ResourceObject resOb, bool single= false)
    {
        var pane = new GameObject();
        var img = pane.AddComponent<Image>();
        img.sprite = resOb.icon;
        pane.transform.SetParent(gameObject.transform);
        img.rectTransform.sizeDelta = new Vector2(35, 35);
        if (!single)
        {
            requiredRessources.Add(pane);
        }
        else
        {
            outPutGO = pane;
        }
    }

    private void DeleteChilds()
    {
        foreach (Transform child in transform)
        {
            if (child.name == "New Game Object")
            {
                Destroy(child.gameObject);
            }
        }
    }

    private void ResetIcons()
    {
        requiredRessources.Clear();
        DeleteChilds();
        if (parentRessouces!= null) {
            foreach (ResourceObject res in parentRessouces.availableIn)
            { AppendIcons(res); }
            AppendIcons(outPutResource, true);
            text.text = "Requires: \n\n\nYields:\n" + parentRessouces.availableOut + "x";
        }
    }
}
