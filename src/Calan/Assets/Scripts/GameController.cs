using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using TMPro;

public class GameController : MonoBehaviour
{
    [HideInInspector]
    public Galaxy CurrentyClickedGalaxy;
    public Galaxy HoveredPlanet;
    public SpaceShipSpawner SpawnerPrefab;
    public SpaceShip UiMovingIconFrefab;
    private GameObject _UiFleetCount;

    public Material StarLinkMaterial;

    private List<StarLink> _starLinks = new List<StarLink>();
    
    public static GameController Instance;
    private HUDController _hudController;
    public ResourceObject[] PlanetResources;

    private int _establishedLinks;

    private int _highScore;
    private int _fullfilledNeeds;
    public int AvailableLinks;

    public bool IsPaused = false;
    public GameObject PauseMenu;
    public GameObject GameOverMenu;

    private bool CanSelectAllPlanets = true;

    public bool FogEnabled;

     void Awake() {
        Instance = this;
        PlanetResources = Resources.LoadAll<ResourceObject>("IngameResources");
        FogEnabled = FindObjectOfType<FogController>() != null;
    }

    // Start is called before the first frame update
    void Start()
    {
        _UiFleetCount = GameObject.Find("FleetCounter");
        AvailableLinks = 5;
        _hudController = FindObjectOfType<HUDController>();
        _hudController.UpdateLinksAvailable(AvailableLinks, _establishedLinks);
        UpdateFleetCount();
        IsPaused = GameObject.Find("Tutorial") != null;
    }

    void UpdateFleetCount() {
        _UiFleetCount.GetComponent<TextMeshProUGUI>().text = "Ships: " + FindObjectsOfType<SpaceShip>().Length;
        Invoke("UpdateFleetCount", 0.1f);
        
    }

    public void CopyToClipboard() {
        var str = "I reached following stats in W.O.O.T:\nScore: " + _highScore + "\nShips: " + FindObjectsOfType<SpaceShip>().Length + "\nLinks: " + _establishedLinks + "\nI had so much fun playing this awesome game";
        GUIUtility.systemCopyBuffer = str;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape)) {
            TogglePause();

        }

        if (Input.GetMouseButtonDown(1) || IsPaused) {
            CurrentyClickedGalaxy?.ResetAlpha();
            CurrentyClickedGalaxy = null;
        }        
    }

    public void AddToScore(int points) {
        _highScore += points;
        _hudController.UpdateScore(_highScore);
    }

    public void ResourceFullfilled(string resourceName) {
        _fullfilledNeeds +=1;
        AvailableLinks += 1;
        _hudController.UpdateLinksAvailable(AvailableLinks, _establishedLinks);
    
        _hudController.IncreaseResourceDisplay(resourceName);
    }


    public void TogglePause() {
        SetPause(!IsPaused);
    }

    public void LoadMainMenu() {
        SceneManager.LoadScene("MainMenu");
    }
    
    public void SetPause(bool state) {
        // Hide all tooltips
        foreach(var tooltip in  FindObjectsOfType<ToolTip>()) {
            tooltip.Hide();
        }
        PauseMenu?.SetActive(state);
        GameOverMenu.SetActive(false);
        IsPaused = state;
    }

    public void RegisterGalaxyClick(Galaxy clickedGalaxy) {
        if (IsPaused) {
            return;
        }
        if (AvailableLinks == 0) {
            return;
        }
        // Only allow selection of already connected planets.
        if (!CanSelectAllPlanets && CurrentyClickedGalaxy == null && !clickedGalaxy.IsConnectedToOtherPlanet()) {
            return;
        }

        if (CurrentyClickedGalaxy == null) {         
            CurrentyClickedGalaxy = clickedGalaxy;
            clickedGalaxy.ChangeAlpha();
        }
        // Only draw line if different galaxy is clicked
        else if (CurrentyClickedGalaxy != clickedGalaxy) {
            HandleStarlinkAttempt(CurrentyClickedGalaxy, clickedGalaxy);
            CurrentyClickedGalaxy?.ResetAlpha();
            CurrentyClickedGalaxy = null;
        }
    }

    private void HandleStarlinkAttempt(Galaxy galaxy1, Galaxy galaxy2) {
        var existingLink = GetExistingStarLink(galaxy1, galaxy2);

        if (existingLink == null) {
            // Create new link
            var link = new GameObject("StarLink").AddComponent<StarLink>();
            link.CreateBetween(galaxy1, galaxy2);
        }

        // Link Removal disabled;
        // else {            
        //     existingLink.Remove();
        // }
    }

    public void RegisterSuccessfullLink() {
        CanSelectAllPlanets = false;
        AvailableLinks -= 1;
        _establishedLinks += 1;
        _hudController.UpdateLinksAvailable(AvailableLinks, _establishedLinks);

        if (AvailableLinks == 0) {    
            HandleGameOver();       
        }
        AudioController.Instance.PlaySound("Positive_Pop_04");
    }

    private void HandleGameOver() {
        // Hide all tooltips
        foreach(var tooltip in  FindObjectsOfType<ToolTip>()) {
            tooltip.Hide();
        }
        GameOverMenu.SetActive(true);
        IsPaused = true;
        GameOverMenu.transform.Find("Score").GetComponent<TextMeshProUGUI>().text = _highScore.ToString();

    }

    // Probably inefficient.
    private StarLink GetExistingStarLink(Galaxy galaxy1, Galaxy galaxy2) {
        var starLinks = FindObjectsOfType<StarLink>();
        foreach (var link in starLinks)
        {
            // We can test like this, because links with same start and endpoint are forbidden.
            if (galaxy1 == link.StartPoint || galaxy1 == link.EndPoint) {
                if (galaxy2 == link.StartPoint || galaxy2 == link.EndPoint) {
                    return link;
                }
            }            
        }
        return null;
    }

    public static void LoadMainGame() {        
        SceneManager.LoadScene("GameScene");
    }
        // Quit game
    public static void ExitGame() {
        Debug.Log("Quit Game");
        Application.Quit();
    }

}
