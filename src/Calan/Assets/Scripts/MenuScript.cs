using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public Button pressToStart;
    public Button start;
    public Button options;
    public Button credits;
    public Button quit;
    public Button exitOptions;
    public Button exitCredits;

    private GameObject Menu;
    private GameObject OptionsMenu;
    private GameObject CreditsScreen;


    void Start()
    {
        Menu = GameObject.Find("Menu");
        OptionsMenu = GameObject.Find("OptionsMenu");
        CreditsScreen = GameObject.Find("CreditsScreen");

        Menu.SetActive(false);
        OptionsMenu.SetActive(false);
        CreditsScreen.SetActive(false);

        Button btn0 = pressToStart.GetComponent<Button>();
        Button btn1 = start.GetComponent<Button>();
        Button btn2 = options.GetComponent<Button>();
        Button btn3 = credits.GetComponent<Button>();
        Button btn4 = quit.GetComponent<Button>();
        Button btn5 = exitOptions.GetComponent<Button>();
        Button btn6 = exitCredits.GetComponent<Button>();


        btn0.onClick.AddListener(OpenMenu);
        btn1.onClick.AddListener(OpenGameSetup);
        btn2.onClick.AddListener(OpenOptions);
        btn3.onClick.AddListener(OpenCredits);
        btn4.onClick.AddListener(QuitGame);
        btn5.onClick.AddListener(ExitOptions);
        btn6.onClick.AddListener(ExitCredits);

    }

    void OpenMenu()
    {

        GameObject.Find("PressToStart").SetActive(false);
        Menu.SetActive(true);
    }

    void OpenGameSetup()
    {
        Menu.SetActive(false);  // HIER STARTET DAS SPIEL
        GameController.LoadMainGame();
        
        // ...
    }

    void OpenOptions()
    {
        Menu.SetActive(false);
        OptionsMenu.SetActive(true);
    }

    void OpenCredits()
    {
        Menu.SetActive(false);
        CreditsScreen.SetActive(true);
    }

    void QuitGame()
    {
        Application.Quit();
    }

    void ExitOptions()
    {
        OptionsMenu.SetActive(false);
        Menu.SetActive(true);
    }

    void ExitCredits()
    {
        CreditsScreen.SetActive(false);
        Menu.SetActive(true);
    }
}
