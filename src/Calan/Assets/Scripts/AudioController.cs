using System.Collections.Generic;
using UnityEngine;

// ReSharper disable once CheckNamespace
public class AudioController : MonoBehaviour
{
    public static AudioController Instance;

    [Range(0, 10)]
    public int MusicVolume;
    [Range(0, 10)]
    public int SfxVolume;

    private Sound _currentMusicTrack;

    public List<Sound> MusicTracks;
    public List<Sound> SfxSounds;

    private Dictionary<string, int> _lastNotes = new Dictionary<string, int>();

    // ReSharper disable once UnusedMember.Local
    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        // Keep existing controller from different scene.
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            foreach (var sound in SfxSounds)
            {
                try
                {
                    sound.Stop();
                }
                catch
                {
                    // ignored
                }
            }
            Destroy(gameObject);
            return;
        }

        MusicVolume = PlayerPrefs.GetInt("musicVolume", 5);
        SfxVolume = PlayerPrefs.GetInt("sfxVolume", 5);
        LoadSounds();
    }

    void Start() {
        
        PlayMusic("Themes_merged");
    }


    // Dynamically loads all sounds and music tracks from the scriptable objects from the Resource folder.
    private void LoadSounds()
    {
        var musicSounds = Resources.LoadAll<Sound>("Audio/Music");
        var sfxSounds = Resources.LoadAll<Sound>("Audio/Sfx");


        foreach (var sound in musicSounds) {
            sound.Source = gameObject.AddComponent<AudioSource>();
            sound.Source.clip = sound.Clip;
            sound.Source.volume = sound.Volume;
            sound.Source.loop = sound.Loop;
            MusicTracks.Add(sound);
        }

        foreach (var sound in sfxSounds) {
            sound.Source = gameObject.AddComponent<AudioSource>();
            sound.Source.clip = sound.Clip;
            sound.Source.volume = sound.Volume;
            sound.Source.loop = sound.Loop;
            SfxSounds.Add(sound);
        }
    }


    // Plays a music track and stops the already playing one.
    public void PlayMusic(string soundName)
    {
        var sound = MusicTracks.Find(i => i.Clip.name == soundName);
        Play(sound, (float)MusicVolume / 10, soundName);
        if (sound != null)
        {
            _currentMusicTrack?.Source.Stop();
            _currentMusicTrack = sound;
        }
    }

    // Change Sound Volume.
    public void SetSoundVolume(string soundName, float volume)
    {
        var sound = GetSound(soundName);
        sound.SetMasterVolume(SfxVolume * volume);
    }

    // Find sfx Sound by Name
    public Sound GetSound(string soundName)
    {
        return SfxSounds.Find(i => i.Clip.name == soundName);
    }

    // Play  sfx Sound by Name
    public void PlaySound(string soundName, float volume = 1f)
    {
        var sound = GetSound(soundName);
        Play(sound, volume * SfxVolume / 10, soundName);
    }

    // Stops a sfx sound by name.
    public void StopSound(string soundName)
    {
        var sound = GetSound(soundName);
        sound?.Stop();
    }

    // Plays a audioclip
    private void Play(Sound sound, float volume, string soundName)
    {
        if (sound != null)
        {
            sound.Play(volume);
        }
        else
        {
            Debug.LogError("Clip " + soundName + " not found for playback");
        }
    }

    // Dynamic Volume depending on camera distance
    public void PlaySoundRelativeToCameraPosition(string soundName, Vector3 soundOriginPosition)
    {
        var cameraController = FindObjectOfType<Camera>();
        if (!cameraController)
        {
            Debug.LogError("No Camera Controller found");
            return;
        }
        var distanceFromCamera = Vector3.Distance(cameraController.transform.position, soundOriginPosition);

        var farDistance = 4.8f;
        var closeDistance = 4.2f;


        var distanceSfxVolumeFactor = 1 - Mathf.Max(0, Mathf.Min((distanceFromCamera - closeDistance) / (farDistance - closeDistance), 1));

        // Sound Loudness with dezibel stuff
        var modifiedVolume = Mathf.Pow(Mathf.Clamp01(distanceSfxVolumeFactor), Mathf.Log(10, 4));

        PlaySound(soundName, modifiedVolume);
    }

    // Stops all sfx sounds from playing
    public void StopAllSfxSounds()
    {
        foreach (var sound in SfxSounds)
        {
            sound.Stop();
        }
    }

    // Change sound of music sound.
    public void ChangeMusicVolume(int change)
    {
        MusicVolume += change;
        EnsureMusicIntervals();
        _currentMusicTrack?.SetMasterVolume((float)MusicVolume / 10);
        PlayerPrefs.SetInt("musicVolume", MusicVolume);
    }

    // Change sound of Sfx sounds
    public void ChangeSfxVolume(int change)
    {
        SfxVolume += change;
        EnsureMusicIntervals();
        foreach (var sound in SfxSounds)
        {
            sound.SetMasterVolume((float)SfxVolume / 10);
        }
        PlayerPrefs.SetInt("sfxVolume", SfxVolume);
    }


    // Static Wrapper for Music Volume setting
    public void SetMusicVolumeSlider(float value) {
        Instance.SetMusicVolume((int) (value * 10));
    }

    // Static Wrapper for Sfx Volume setting
    public void SetSfxVolumeSlider(float value) {
        Instance.SetSfxVolume((int) (value * 10));
    }

    // Sets the music level and stores it in player prefs
    public void SetMusicVolume(int change)
    {
        MusicVolume = change;
        EnsureMusicIntervals();
        _currentMusicTrack?.SetMasterVolume((float)MusicVolume / 10);
        PlayerPrefs.SetInt("musicVolume", MusicVolume);
    }

    // Sets volume of sfx sounds
    public void SetSfxVolume(int change)
    {
        SfxVolume = change;
        EnsureMusicIntervals();
        foreach (var sound in SfxSounds)
        {
            sound.SetMasterVolume((float)SfxVolume / 10);
        }
    }

    // Stops invalid sound volumes from being stored.
    private void EnsureMusicIntervals()
    {
        int minVolume = 0;
        int maxVolume = 10;

        MusicVolume = Mathf.Min(maxVolume, Mathf.Max(minVolume, MusicVolume));
        SfxVolume = Mathf.Min(maxVolume, Mathf.Max(minVolume, SfxVolume));
        PlayerPrefs.SetInt("musicVolume", MusicVolume);
        PlayerPrefs.SetInt("sfxVolume", SfxVolume);
    }
    
    // Static Wrapper for playing a menu click
    public static void PlayMenuClick() {
        Instance.PlaySound("menuClick");
    }

    // Selects a random tone and plays it. Double playing prevented (inefficient)
    public void PlayRandomSound(string prefix, int soundCount = 8, bool preventRepeat = true) {
        int newNote;
        int lastNote = _lastNotes.GetValueOrDefault<string, int>(prefix, -1);
        do {
            newNote = Random.Range(0,soundCount);
        } while(preventRepeat && newNote == lastNote);
        _lastNotes[prefix] = newNote;
        PlaySound(prefix + newNote);
    }

}
