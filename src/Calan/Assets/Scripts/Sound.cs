using UnityEngine;

[CreateAssetMenu(fileName = "New Sound", menuName = "ScriptableObjects/Sound")]
// ReSharper disable once CheckNamespace
public class Sound : ScriptableObject
{
    [HideInInspector]
    public AudioSource Source;
    public AudioClip Clip;

    [Range(0, 1)]
    public float Volume = 1;

    public bool Loop;

    // Plays the sound.
    public void Play(float masterVolume)
    {
        if (Source != null)
        {
            Source.volume = Volume * masterVolume;
            Source.Play();
        }
    }

    // Change sound level.
    public void SetMasterVolume(float masterVolume)
    {
        Source.volume = Volume * masterVolume;
    }

    // Stop sound from playing
    public void Stop()
    {
        Source.Stop();
    }

}
