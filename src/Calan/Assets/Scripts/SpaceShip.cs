using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShip : MonoBehaviour
{
    public Vector3 StartLocation;
    public Vector3 EndLocation;
    public float MinSpeed = 1f;
    public float MaxSpeed = 3f;
    
    public float Scale = 1;
    public float SpawnRadius = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        MinSpeed = Random.Range(0.5f*MinSpeed, 1.5f*MinSpeed);
        MaxSpeed = Random.Range(0.5f*MaxSpeed, 1.5f*MaxSpeed);
        
        StartLocation = StartLocation + new Vector3(1, 1,0) * Random.Range(-SpawnRadius, SpawnRadius);
        EndLocation = EndLocation + new Vector3(1, 1,0) * Random.Range(-SpawnRadius, SpawnRadius);
        transform.localScale *= Scale;
        transform.position = StartLocation;
        transform.right = transform.position - EndLocation;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.Instance.IsPaused) { return;}
        var speed = CalcSpeedCurrentPosition();
        var step =  speed * Time.deltaTime; // calculate distance to move
        transform.position = Vector3.MoveTowards(transform.position, EndLocation, step);
        CheckDestinationReached();
    }

    private void CheckDestinationReached() {
        if ((EndLocation - transform.position).magnitude < 0.01f) {
            Destroy(gameObject);
        }
    }

    private float CalcSpeedCurrentPosition() {
        var fullDistance = (EndLocation - StartLocation).magnitude;
        var halfDistance = fullDistance / 2;
        var currentDistance = (EndLocation - transform.position).magnitude;

        var ratio = 1 - (Mathf.Abs(currentDistance-halfDistance) / halfDistance);

        return Mathf.Lerp(MinSpeed, MaxSpeed, ratio);

    }
}
