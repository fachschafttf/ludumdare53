using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Planet", menuName = "ScriptableObjects/Planet")]
public class SO_Planet : ScriptableObject
{
    public ResourceObject OutputResource;
    public Sprite Texture;   
}
